// Copyright Epic Games, Inc. All Rights Reserved.

#include "MetroDispatch.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MetroDispatch, "MetroDispatch" );
